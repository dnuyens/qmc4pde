DATE=`date "+%Y%m%d"`
cd .. # step one directory down such that the whole archive is one directory
COPYFILE_DISABLE=1 tar czf qmc4pde/qmc4pde-$DATE.tgz \
                              qmc4pde/index.html          \
                              qmc4pde/spod-cbc.py         \
                              qmc4pde/lat-cbc.py          \
                              qmc4pde/polylat-cbc.py      \
                              qmc4pde/qmc4pde/__init__.py \
                              qmc4pde/qmc4pde/lat.py      \
                              qmc4pde/qmc4pde/polylat.py
