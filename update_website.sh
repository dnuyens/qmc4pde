DATE=`date "+%Y%m%d"`
rsync  --delete -avzh \
    qmc4pde-$DATE.tgz \
    index.html               \
    sobolmats.tgz            \
    spod-cbc.py              \
    dirkn@ssh1.cs.kuleuven.be:public_html/qmc4pde/
rsync  --delete -avzh \
    qmc4pde/lat.py           \
    qmc4pde/polylat.py       \
    dirkn@ssh1.cs.kuleuven.be:public_html/qmc4pde/qmc4pde/

