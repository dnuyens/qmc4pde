# The QMC4PDE package: Constructing QMC point sets for PDEs with random
# diffusion coefficients (and other applications)

This is a git repository with the Python code for the component-by-component
construction scripts to construct generating vectors (for lattice rules and
sequences) and generating matrices (for interlaced polynomial lattice rules)
aimed at calculating the expected value over the solution of a PDE with a
random field as the diffusion coefficient. The integrand functions in this case
are infinite dimensional.

The main page for this software package is
  [https://people.cs.kuleuven.be/~dirkn/qmc4pde/](https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/).
There you can also find the additional data file for the construction of the
interlaced polynomial lattice rules
  [https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/qmc4pde/polylat-data.tgz](https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/qmc4pde/polylat-data.tgz).
This file needs to be unpacked inside the `qmc4pde` directory where the file
`polylat.py` resides.

The construction scripts can in fact be used for any integrand which adheres to
the specific SPOD form that we assume. See the main web page for an explanation
of that form and which parameters occur. Those parameters can then be set as
command line parameters for the construction scripts. There is also a command
line option called `-h` which lists the possible options. Although the setting
is for infinite dimensional integrand functions, this is not a requirement.

The interlaced polynomial lattice rules can be used for integration over the
unit cube (or any affine transformation to the unit cube), and, given the right
smoothness can provide algebraic convergence in terms of the number of sample
points, like N^{-2} and N^{-3}.

The lattice rules and sequences can be used for integration over the unit cube
(or any affine transformation to the unit cube) and additionally for
integration over the whole space R^s against the normal distribution. In the
latter case the parameters for this setting have to be provided to the
construction script.

The main construction script is `spod-cbc.py`, its two siblings `lat-cbc.py`
and `polylat-cbc.py` are just symlinks to `spod-cbc.py`.

To be able to use the constructed generating vectors and generating matrices
one can make use of the Magic Point Shop point generators.
The code for those are provided in their own git repository:
  [https://bitbucket.org/dnuyens/qmc-generators](https://bitbucket.org/dnuyens/qmc-generators).
There are Python2/3, Matlab/Octave and C++ flavours of those.
More explanation on these generators can also be found on the main page for the
  [Magic Point Shop](https://people.cs.kuleuven.be/~dirk.nuyens/qmc-generators/).

## Downloading or cloning this repository

If you know how to work with `git` you can `clone` this repository by doing

    git clone --depth 1 https://dnuyens@bitbucket.org/dnuyens/qmc-generators.git

The `--depth 1` option makes it that you only get the latest version in the
repository and saves you some time and space. If you have a cloned version of
the repository and you want to update it you can do that by `fetch` in the
repository directory:

    git fetch

Much easier is to just download the current version by clicking the
download link on the left.

## Using this package

Currently there is no install script yet. If you want to use this package
locally then you need to make sure that you have the required libraries
from `requirements.txt` installed. E.g. if you are using anaconda this
will probably be satisfied. For a standard Python installation you can
make a virtual configuration, activate it and then install the `requirements.txt`
file using `pip`.

Currently the contents of `requirements.txt` is are the following packages:
- `numpy`
- `scipy`
- `mpmath`

## Acknowledging

This software is written and maintained by Dirk Nuyens.

Please acknowledge this software if you use it in your research by citing

- F. Y. Kuo and D. Nuyens. Application of quasi-Monte Carlo methods to elliptic
  PDEs with random diffusion coefficients -- a survey of analysis and
  implementation. Foundations of Computational Mathematics, 16(6):1631-1696,
  2016.


