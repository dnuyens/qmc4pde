#!/usr/bin/env python

####
## (C) Dirk Nuyens, KU Leuven, 2015,2016,2017,2024,...
## See https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/

from __future__ import print_function
import getopt, sys, os, time
from textwrap import dedent

import numpy as np

cmd_line_ok = True
error_msgs = [ '' ]

f = None # config file

s = None # nb of dimensions
m = None # nb of points is 2^m, or in case of lat: p^m
m1 = 1 # for extensible lattice rule: over which range to look at wce: 2^m1 .. 2^m
p = None # for polylat: primitive polynomial over Z_2 (defaults exist for m from 2 to 30); for lat: prime number base (defaults to 2)
alpha = None # for polylat: interlacing factor; for lat: reserved for future use
a1 = 0 # integer offset for factorial
a2 = 1 # multiplicative scaling for the bounds
a2_string = None
a3 = 0 # boundary behaviour on the cube for lognormal, a3=0 for uniform setting on the cube
lognormal = False
d1 = 1 # power on the factorial part, for d1=0 we get product weights
d2 = None # decay of the derivative bounds
c = None # for parametrized weights of the form c j**-d2
b = None # derivative bounds can be specified as a Python expression with access to j, v (for nu), alpha and s
b_function_str = None # this would then hold the string
b_file = None # derivative bounds specified as numerical values in an input file
out = None # directory to store output files of the construction
force = False # force overwrite of directory
printFullTable = False # in case of lattice sequence: print full table of errors for each power of 2 instead of just the highest power
lambda_ = None # the lambda parameter in the convergence of lattice rules/sequences

Cwalsh = 1 # bound for the Walsh coefficients, defaults to 1 (which is the proven bound for base 2)
MP = None # use multi precision with this amount of decimal digits precision
pointset = None # specify if this is a 'lat'  or a 'polylat' point set
power_2_fft = True # do the FFTs on extended vectors of size power-of-2 using symmetry and padding
delta = 0.125 # delta which controls the choice of lambda in the construction of lattice rules (how close we get to the optimal convergence)
qstar = None # the maximum order to consider for lattice rule construction with order dependent weights (TODO for poly-lat)

p_set_before = None
m_set_before = None
p_set_in_configfile = None
m_set_in_configfile = None

try:
    opts, files = getopt.getopt(sys.argv[1:], '',
                         ['s=',
                          'm=',
                          'm1=',
                          'p=',
                          'alpha=',
                          'f=','configfile=',
                          'out=','outputdir=',
                          'Cwalsh=',
                          'a1=',
                          'a2=',
                          'a3=',
                          'd1=',
                          'd2=',
                          'c=',
                          'b=',
                          'b_file=','b-file',
                          'delta=',
                          'MP=','mp=',
                          'power_2_fft','power-2-fft',
                          'no_power_2_fft','no-power-2-fft',
                          'pointset=',
                          'force',
                          'printFullTable',
                          'lambda=',
                          'qstar='])
    for opt, arg in opts:
        #print opt, arg
        if   opt in ('--s'): s = int(arg)
        elif opt in ('--m'): m = int(arg)
        elif opt in ('--p'): p = int(arg)
        elif opt in ('--m1'): m1 = int(arg)
        elif opt in ('--alpha'): alpha = int(arg)
        elif opt in ('--f', '--configfile'):
            f = arg
            # we need to take care if m and p are listed in the config file
            # the user still needs to be able to override this on the command line (after the --f arg)
            p_set_before, m_set_before, p, m = p, m, None, None
            # if p_set and m_set contain values, that looks like a user mistake...
            exec(compile(open(f).read(), f, 'exec')); # we execute immediately, so overrides come after this argument!
            p_set_in_configfile, m_set_in_configfile, p, m = p, m, None, None
            # need to fix some float values if specified as strings:
            if a2: # a2 could be specfied as a Python expression, we save the expression and evaluate it
                try:
                    a2 = float(arg)
                except ValueError:
                    a2_string = arg
            if a3: a3 = float(a3)
            if d1: d1 = float(d1)
            if d2: d2 = float(d2)
            if Cwalsh: Cwalsh = float(Cwalsh)
            if c: c = float(c)
            if delta: delta = float(delta)
        elif opt in ('--out', '--outputdir'): out = arg
        elif opt in ('--Cwalsh'): Cwalsh = float(arg)
        elif opt in ('--a1'): a1 = int(arg)
        elif opt in ('--a2'): # a2 could be specfied as a Python expression, we save the expression and evaluate it
            try:
                a2 = float(arg)
            except ValueError:
                a2_string = arg
        elif opt in ('--a3'): a3 = float(arg)
        elif opt in ('--d1'): d1 = float(arg)
        elif opt in ('--d2'): d2 = float(arg)
        elif opt in ('--c'): c = float(arg)
        elif opt in ('--b'): b_file = None; b_function_str = arg # this overrides any prev choice of b_file
        elif opt in ('--b_file', '--b-file'): b = None; b_file = arg # this overrides any prev choice of b
        elif opt in ('--delta'): delta = float(arg)
        elif opt in ('--MP', '--mp'): MP = int(arg)
        elif opt in ('--power_2_fft','--power-2-fft'): power_2_fft = True
        elif opt in ('--no_power_2_fft','--no-power-2-fft'): power_2_fft = False
        elif opt in ('--pointset'): pointset = arg
        elif opt in ('--force'): force = True
        elif opt in ('--printFullTable'): printFullTable = True
        elif opt in ('--lambda'): lambda_ = float(arg)
        elif opt in ('--qstar'): qstar = int(arg)
        else: cmd_line_ok = False
except getopt.GetoptError as e:
    error_msgs.append("Error: " + str(e))
    cmd_line_ok = False

from qmc4pde import polylat, lat

# check which pointset we need to construct
if not pointset:
    # check what the name of this script is to deduce if we want a lat or a polylat
    # nb: we do allow abuse of a script with name lat-cbc to construct polylat...
    if "polylat" in sys.argv[0]: pointset = "polylat"
    elif "lat" in sys.argv[0]: pointset = "lat"
    else:
        error_msgs.append("Error: please provide the argument --pointset={lat|polylat} as it couldn't be deduced from the command name.");
        cmd_line_ok = False

# fix up overrides of m and or p over a configfile
if not m and not p and m_set_in_configfile and p_set_in_configfile:
    m, p = m_set_in_configfile, p_set_in_configfile
if not m and not p and m_set_before and p_set_before:
    msg = "Warning: you are supposed to put overrides later on the command line, cfr, m and p to override configfile m and p."
    sys.stderr.write(msg + "\n");
    error_msgs.append(msg) # append this to error messages such that it will be repeated after the help text if other inconsistencies are detected
    m, p = m_set_before, p_set_before

# set default values for alpha
if pointset == 'lat' and not alpha: alpha = 1
if pointset == 'polylat' and not alpha: alpha = 2
# check validity of alpha
if pointset == 'lat' and alpha != 1: 
    error_msgs.append('Error: currently alpha needs to be 1 for lattice rules!')
    cmd_line_ok = False
if pointset == 'polylat' and alpha < 2: 
    error_msgs.append('Error: currently we need alpha >= 2 for interlaced polynomial lattice rules!')
    cmd_line_ok = False

# there are multiple options to specify the product part in the derivative bounds
# eventually we want b to be acting like a function, ie, be callable
# we take care of the multiple possibilities and that logic here:
b_fun = None
if b and not hasattr(b, '__call__'): # this case could happen when b is defined in a config file as a Python expression
    b_function_str = b
    b = None
if b and hasattr(b, '__call__'):
    import inspect
    b_fun = inspect.getsource(b)
    b_source_file = inspect.getsourcefile(b)
if not b and b_function_str: # this is when b is defined as Python expression on cmd line
    b = eval("lambda j, v=1: " + b_function_str)
if not b and not b_file and d2: # this is we want the default parametrized form a2 * j^-d2
    if not c: c = 1
    print("setting b = c * j**-d2 with d2 =", d2, " and c =", c)
    b = lambda j, v=1: c*j**-d2
if not b and b_file: # this case when the user provides a file
    bdata = np.loadtxt(b_file)
    if len(bdata) < s:
        sys.stderr.write("Error: the bounds file %s does not contain enough data for s=%d\n" % (b_file, s))
        sys.exit(1)
    b = lambda j, v=1: bdata[j-1]

# check p and m
if pointset == 'polylat':
    if p and m and not (p >> m == 1):
        error_msgs.append("Error: the degree of your irreducible polynomial p does not equal the given m.")
        cmd_line_ok = False
    if p and not m:
        m = polylat.floorlog2(p)
    if not p and m and m >= 2 and m <= 30:
        p = polylat.default_base2_polys[m]
elif pointset == 'lat':
    if not p: p = 2
    if m and not m >= 1:
        sys.stderr.write("Error: the power m for the number of points n=p^m should be >= 1.\n")
        sys.exit(1)
else:
    error_msgs.append("Error: pointset must be either lat (for lattice rule) or polylat (for interlaced polynomial lattice rule).")
    cmd_line_ok = False

if not s:
    error_msgs.append("Error: number of dimensions --s missing.");
    cmd_line_ok = False

if not m:
    error_msgs.append("Error: number of points missing as power of the base --m missing.");
    cmd_line_ok = False

if not d2: d2 = 2

if not b:
    error_msgs.append("Error: you did not specify the product part b_j in the derivative bounds.")
    cmd_line_ok = False

if a3: lognormal = True

if lognormal and pointset == 'polylat':
    error_msgs.append("Error: the lognormal case is only supported for lattice rules.")
    cmd_line_ok = False


# check if we have all required arguments
cmd_line_ok = cmd_line_ok and pointset and s and m and p and alpha and d2 and b

if not cmd_line_ok:
    sys.stderr.write("\n" + dedent(r'''
        Construct lattice rules and interlaced polynomial lattice rules for PDE problems with random coefficients where
        the partial mixed deriviatives in terms of the randomness are of SPOD form (see below).

        Usage: %(cmd)s --s {s} --m {m} [--p {p}] [--alpha {alpha}] 
                       [--a1 {a1}] [--a2 {a2}] [--a3 {a3}] [--d1 {d1}] --d2 {d2}
                       [--b {bound-function} | --b_file {file_name} | --c {c}]
                       [--Cwalsh {Cwalsh}] [--delta {delta}] [--out={outputdir}]
           or: %(cmd)s --f {configfile}  in combination with any of the options, what comes later overrides earlier given arguments

        where --s {s}                    the number of dimensions, integer
              --m {m}                    - for polylat: the power of 2 for n=2^m points, integer, 
                                           default polynomials are available for 2 <= m <= 30
                                         - for lat: the power of p for n=p^m points, integer, m >= 1
              --p {p}                    - for poylat: primitive polynomial in integer presentation with deg(p) = m, integer
                                         - for lat: base of the lattice sequence, defaults to 2, prime
              --alpha {alpha}            - for polylat: the interlacing factor, integer >= 2
                                         - for lat: reserved for future use, needs to be 1 for now
              --a1 {a1}                  integer offset for factorial, defaults to 0, integer
              --a2 {a2}                  multiplicative scaling for the derivative bounds, defaults to 1, real or Python expression
              --a3 {a3}                  boundary behaviour on the cube for lognormal, defaults to 0 in which case this is the uniform setting, real
              --d1 {d1}                  power on the factorial part, d1=0 gives product weights, defaults to 1, real
              --d2 {d2}                  the decay of the derivative bounds (power of product part), defaults to 2, real
              --c {c}                    optional, for parametrized product derivative part of the form b_j = c j^-d2
              --b {bound-function}       product part in derivative bounds as Python expression with access to j and v (for v_j)
                                         NOTE that powers are expressed as j**-2 in Python and NOT as j^-2
              --b_file {file_name}       product part of derivative bounds given as numerical values in file
              --out {outputdir}          write files to this directory, defaults to 
                                            output/{pointset}-n{n}-s{s}-m{m}-p{p}-alpha{alpha}-a1_{a1}-a2_{a2:g}-a3_{a3:g}-c_{c}-d1_{d1:g}-d2_{d2:g}
              --pointset {lat|polylat}   explicitly specify to construct a lattice rule (lat) or 
                                         an interlaced polynomial lattice rule (polylat),
                                         defaults to extraction from command name, but can be overridden by this argument as well
              --force                    force output directory to be overwritten if it already exists

        Example: %(cmd)s --s=5 --m=20 --alpha=3 --d2=2 --a2=3 [--pointset=polylat]

        The output will create the following files in the output directory:
        - common output:
          * b.txt           the product part in the derivative bounds as numerical values per dimension
          * E.txt           the worst-case error for the interlaced polynomial lattice rule for dimensions 1,...,s
          * config.txt      a file specifying all parameters and command line options (could be used with --f)
        - for lat:
          * z.txt           a file containing the generating vector of the lattice
          * lat.txt         a file with the first three lines giving n, p, m and then the generating vector over the next lines
        - for polylat:
          * Bs.col, Bs53.col, Bs64.col          
                            the generating matrices of the interlaced polynomial lattice in column format as integers
                            (for usage with multiprecision, double precision and long double precision point generators respectively)
          * Cs.col          the generating matrices of the alpha * s (non-interlaced) polynomial lattice in column format as integers
          * z.poly          the polynomials in integer representation of the polynomial lattice

        This generates points for a function f which satisfies the "SPOD" bound on it's derivatives
          | (\partial^v_y f)(y_1, ..., y_s) | = O(   ((|v|+a_1)!)^{d_1}  \prod_{j=1}^s (a_2 b_j)^{v_j} \exp(a_3 b_j |y_j|)  )
        for all v in {0, 1, ..., \alpha}^s and all y in [0,1]^s.

        For the product part in the derivatives one has to specify a sequence b_j. There are four main ways of doing this:
        1. If none of the options --b and --b_file are given then b_j takes the parametrized form
              b_j = c j^{-d_2} .
        2. The option --b can be used to specify a Python expression using the variables j and v (where v represents v_j), e.g.
              --b "0.1 * j**-3 * log(j)"   [-d2=-3]    # best to specify d2 in case of lattice rules
           this will then be turned into a lambda function using eval. One has to be aware that in the case
           of multiprecision the constant 0.1 above will be first read as float and then upgraded to multiprecision,
           which means it will not equal mpmath.mpf("0.1"). For practical applications that will probably not be a 
           problem as the b_j are supposed to be bounds anyway. For numerical testing one might want to represent
           the above argument as "j**-3 * log(j) / 10" instead. The argument j is always first cast to the underlying
           float type of the construction (being float or mpmath.mpf). Also in case of multiprecision the log function
           will have been imported from MPmpmath and the above will thus bind to the correct (multiprecision) version.
        3. The option --b_file can be used to specify a file which contains numerical values for the b_j (as floats).
        4. It is also possible to define a b function with argument list (j, v) (where v represent v_j) in the 
           configuration file.

        The last line of stdout lists the worst-case error for dimension 1,...,s preceded with m.

        (C) Dirk Nuyens, KU Leuven, 2015,2016,2024,...

        %(error_msgs)s
    ''' % {'cmd': sys.argv[0], 'error_msgs': "\n".join(error_msgs)}).strip() + "\n\n")
    sys.exit(1)

# prepare for possible multiprecision calculations:
# we load the functions we need in the global namespace
if MP:
    import mpmath
    from mpmath import *
    mpmath.mp.dps = MP
    float_t = mpmath.mpf
    float_dtype = np.object
else:
    from math import *
    float_t = float
    float_dtype = float

if a2_string: a2 = eval(a2_string) # evaluated in mp if MP set

# print some info to stdout about the construction we are going to run:
if pointset == 'polylat': n_base = 2
else: n_base = p
n = n_base**m
if out: outputdir = out
else: 
    a2f = float(a2)
    outputdir = os.path.join('output', '{pointset}-n{n}-s{s}-m{m}-p{p}-alpha{alpha}-a1_{a1}-a2_{a2f:g}-a3_{a3:g}-c_{c}-d1_{d1:g}-d2_{d2:g}'.format(**locals()))
print("pointset={pointset}, s={s}, m={m}, p={p}, n={n_base}^m={n}, alpha={alpha}\n\
a1={a1}, a2={a2}, a3={a3}, d1={d1}, d2={d2}, c={c}\n\
b={b_function_str}, b_file={b_file}, out={outputdir}".format(**locals()))
print("b =", b(float_t(1)), end=' ') 
for j in range(2,min(3,s)+1): print(",", b(float_t(j)), end=' ')
if s > 4: print(", ...", end=' ')
if s > 3: print(",", b(float_t(s)))
else: print()

if not os.path.exists(outputdir):
    os.makedirs(outputdir)
elif not force:
    print(('\n***!!!##### The output directory %s exists, you have 2 seconds to press ctrl-C if you want to cancel\n' % outputdir))
    time.sleep(2)

# our own save to file as numpy savetxt seems to have trouble with too large ints (casting them to floats and loosing bits)
def save_to_file(filename, Bs):
    f = open(filename, 'w')
    for mat in Bs:
        for col in mat:
            print(col, end=' ', file=f)
        print(file=f)
    f.close()

elapsed_time0 = os.times()[4]

if pointset == 'polylat':
    tau_alpha = 2**(alpha*(alpha-1)/2)
    print("Cwalsh={}, tau_alpha={}".format(Cwalsh, tau_alpha))
    (Bs, Cs, z, E, p) = polylat.fastinterlacedpoly2rank1spod(
                                    s, alpha, b, m, p, d1, a1, a2, a3,
                                    Cwalsh=Cwalsh, tau_alpha=tau_alpha, 
                                    float_t=float_t, float_dtype=float_dtype, 
                                    power_2_fft=power_2_fft)[:5]
    Bs53 = [ [ Bjc % 2**53 for Bjc in Bj ] for Bj in Bs ]
    Bs64 = [ [ Bjc % 2**64 for Bjc in Bj ] for Bj in Bs ]
    #np.savetxt('%s/Bs53.col' % outputdir, Bs53, fmt='%d')
    #np.savetxt('%s/Bs64.col' % outputdir, Bs64, fmt='%d') # numpy scrambles the LSBs in this way...
    #np.savetxt('%s/Bs.col'   % outputdir, Bs,   fmt='%d') # this looked correct, but for safety:
    save_to_file('%s/Bs53.col' % outputdir, Bs53)
    save_to_file('%s/Bs64.col' % outputdir, Bs64)
    save_to_file('%s/Bs.col'   % outputdir, Bs)
    np.savetxt('%s/Cs.col'   % outputdir, Cs,   fmt='%d')
    np.savetxt('%s/z.poly'   % outputdir, z,    fmt='%10d')
    np.savetxt('%s/E.txt'    % outputdir, E,    fmt='%.14g')
elif pointset == 'lat':
    if lambda_ == None: lambda_ = lat.propose_lambda(float_t(d2), float_t(delta))
    else: lambda_ = float_t(lambda_)
    print("lambda =", lambda_, ", 1/(2*lambda)=", 1/(2*lambda_))
    Gamma_r = lambda ell: float_t( ell + a1 )**(d1*2/(1+lambda_))
    #print "Gamma =", np.cumprod([ Gamma_r(j) for j in range(1, s+1) ])
    if lognormal:
        gamma = lambda j: lat.gammaR(b(float_t(j)), a2, a3, lambda_)
        alphaj = [ lat.alphaj(b(j), lambda_, a3) for j in range(1, s+1) ]
        omega = [ lambda x, j=j: lat.theta_ab(x, alphaj[j-1]) for j in range(1, s+1) ] # note j=j to have local scope for j (and not current scope)
        omega_cst = [ float_t(lat.theta_c(lat.alphaj(b(j), lambda_, a3))) for j in range(1, s+1) ]
        #print "alphaj =", alphaj
        #print "omegaj ="
        #for f in omega: print [ f(float_t(a/10.)) for a in range(11) ]
        #print "omega_cst =", omega_cst
    else:
        rho = lat.rho(lambda_)
        gamma = lambda j: ( b(float_t(j)) * a2 / sqrt(rho) )**(2/(1+lambda_))
        omega = lambda t: t**2 - t
        omega_cst = 1/6.
    #gammas = [ gamma(j) for j in range(1, s+1) ]
    #print gammas
    (z, e2, x, e2s) = lat.fastrank1expod_base2(s, m, gamma, Gamma_r, omega, omega_cst, m1=m1, qstar=qstar, log="%s/log.txt" % outputdir)[:4] # calculations are just float for now (, float_t=float_t, float_dtype=float_dtype)[:4])
    E = np.sqrt(e2)
    np.savetxt('%s/z.txt'   % outputdir,   z, fmt='%d')
    np.savetxt('%s/E.txt'   % outputdir,   E, fmt='%.14g')
    np.savetxt('%s/e2s.txt' % outputdir, e2s, fmt='%.14g')
    np.savetxt('%s/x.txt'   % outputdir,   x, fmt='%.14g')

elapsed_time1 = os.times()[4]
elapsed_time = elapsed_time1 - elapsed_time0
print()
print("elapsed time = %f s" % (elapsed_time1 - elapsed_time0))
print()
print("outputdir = %s" % outputdir)
print()

# last line is errors in terms of increasing dimensions
print("errors for increasing dimension (prepended by m):")
if pointset == 'polylat':
    print("%d" % m, end=' ')
    for j in range(s): print("%.5g" % E[j], end=' ')
    print()
else:
    if printFullTable: m0 = 0
    else: m0 = m
    for mi in range(m0, m+1):
        print("%2d" % mi, end=' ')
        for j in range(s): print("%.5g" % sqrt(e2s[j,mi]), end=' ')
        print()

# write out configuration file for this run in the output directory:
import time
fid = open('%s/config.txt' % outputdir, 'w')
print("# cmd line = ", " ".join(map(str, sys.argv[:])), file=fid)
print("# elapsed time = %f s" % elapsed_time, file=fid)
print("#", time.strftime("%Y-%m-%d %H:%M:%S"), "local time", file=fid)
print("#", time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()), "UTC", file=fid)
print("pointset =", repr(pointset), file=fid)
print("s =", s, file=fid)
print("m =", m, file=fid)
print("p =", p, file=fid)
print("alpha =", alpha, file=fid)
print("a1 =", a1, file=fid)
if a2_string: print("a2 =", repr(a2_string), file=fid)
else: print("a2 =", a2, file=fid)
print("a3 =", repr(a3), file=fid)
print("d1 =", repr(d1), file=fid)
print("d2 =", repr(d2), file=fid)
if c: print("c =", repr(c), file=fid)
if pointset == 'lat': 
    print("delta =", delta, file=fid)
    print("lambda_ =", lambda_, file=fid)
    print("convrate =", 1/(2*lambda_), file=fid)
if b_function_str: print("b =", repr(b_function_str), file=fid)
if b_file: print("b_file =", repr(b_file), file=fid)
if b_fun: print("\n# b function from ", b_source_file, "\n", b_fun, file=fid)
if qstar: print("qstar =", qstar, file=fid)
if out: print("out =", repr(out), file=fid)
if power_2_fft: print("power_2_fft =", repr(power_2_fft), file=fid)
if pointset == 'polylat': print("Cwalsh =", Cwalsh, file=fid)
if MP: print("MP = ", MP, file=fid)
print("## b =", b(float_t(1)), end=' ', file=fid) 
for j in range(2,min(3,s)+1): print(",", b(float_t(j)), end=' ', file=fid)
if s > 4: print(", ...", end=' ', file=fid)
if s > 3: print(",", b(float_t(s)), file=fid)
else: print(file=fid)
fid.close()

ba = np.zeros((s,), dtype=float)
for j in range(s): ba[j] = b(float_t(j+1))
np.savetxt('%s/b.txt'  % outputdir, ba)

