<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="QMC4PDE: lattice rule and interlaced polynomial lattice rule construction and point generation code targeted at elliptic PDEs with random diffusion coefficients">
    <meta name="author" content="Dirk Nuyens">
    <!--link rel="icon" href="favicon.ico"-->
    <title>QMC4PDE: Quasi-Monte Carlo methods for partial differential equations with random coefficients</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        html, body      { font-family: Verdana, sans-serif; font-size: 15px; line-height: 1.5; }
        body            { position: relative; background-color: #e0e0e0; }
        .section        { padding-top: 40px; padding-bottom: 40px; min-height: 500px; /* color: #fff; */ }
        .section:nth-of-type(even) { background: #e1e8f0; }
        .affix          { top: 0; width: 100%; z-index: 9999 !important; }
        .affix ~ .container-fluid { position: relative; top: 50px; }
        .navbar         { margin-bottom: 0px; }
        .header         { background-color: #f4511e; color: #fff; }
        footer          { background-color: #444; color: #fff; }
        a               { font-weight: bold; }
        a:hover         { font-weight: bold; text-decoration: underline; }
        .ref            { font-weight: bold; }
        td              { vertical-align: top; }
        .section h1     { background-color: #f4511e; color: #fff; width: 100%; border-radius: 10px; padding-left: 10px; padding-right: 10px; }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {
          inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        },
      });
    </script>
    <script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'></script>
  </head>
  <body data-spy="scroll" data-target=".navbar" data-offset="50">

  <div class="container-fluid header">
      <h1><b>QMC4PDE</b></h1>
      <h3>Quasi-Monte Carlo methods for elliptic PDEs with random diffusion coefficients</h3>
      <h4>A practical guide to the software for constructing point sets and point generator code</h4>
      <p>Accompanying the article &ldquo;<em>Application of quasi-Monte Carlo methods to elliptic PDEs with random diffusion coefficients – a survey of analysis and implementation</em>&rdquo;, <i>Foundations of Computational Mathematics</i>, 2016, by Frances Y. Kuo &amp; Dirk Nuyens.</p>
  </div>

  <nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
      <div class="container-fluid">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><b>QMC4PDE</b></a>
          </div>
          <div>
              <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav">
                      <li><a href="#Introduction">Introduction</a></li>
                      <li><a href="#Bound" title="SPOD bounds on mixed partial derivatives in terms of the random parametrization">Bounds</a></li>
                      <li><a href="#RLR" title="Randomly shifted lattice rules">RLR</a></li>
                      <li><a href="#IPLR" title="Interlaced polynomial lattice rules">IPLR</a></li>
                      <li><a href="#ISobol" title="Interlaced Sobol' points">ISobol</a></li>
                      <li><a href="#References">References</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </nav>

  <div style="display: none;">
    \(
        \newcommand{\bst}{\boldsymbol{t}}
        \newcommand{\bsx}{\boldsymbol{x}}
        \newcommand{\bsy}{\boldsymbol{y}}
        \newcommand{\bsz}{\boldsymbol{z}}
        \newcommand{\bsnu}{\boldsymbol{\nu}}
        \newcommand{\bbZ}{\mathbb{Z}}
        \newcommand{\bbR}{\mathbb{R}}
        \newcommand{\bbN}{\mathbb{N}}
        \newcommand{\Bj}{b_j}
        \newcommand{\setu}{\mathfrak{u}}
        \newcommand{\rd}{\,\mathrm{d}}
    \)
  </div>

  <div id="Introduction" class="container-fluid section">
      <h1>Introduction</h1>
<p>
This page provides code which accompanies the article &ldquo;<em>Application of
quasi-Monte Carlo methods to elliptic PDEs with random diffusion coefficients
&mdash; a survey of analysis and implementation</em>&rdquo;, <i>Foundations of
Computational Mathematics</i>, 16(6):1631-1696, 2016, by Frances Y. Kuo &amp; Dirk Nuyens.
</p>
<div class="alert alert-success">
<i>Please cite the article, as well as this web page, if you use the software from this page.</i>
</div>
<p>
  You can download the
  <a href="https://link.springer.com/article/10.1007/s10208-016-9329-5"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-file"></span> published article</button></a>
  or the
    <a href="http://arxiv.org/abs/1606.06613"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-file"></span> ArXiv preprint version</button></a>
  .
</p>

<p>
The code accompanying this article consists of two parts:
</p>
<ol>
    <li>Python scripts for the construction of <a href="#RLR">randomly shifted
        lattice rules (RLR)</a> and <a href="#IPLR">interlaced polynomial
        lattice rules (IPLR)</a> for a specific random parameter setting. This
        is the <a href="">QMC4PDE</a> part.</li>
    <li>Python, Matlab and C++ code for the generation of the points of these
        rules (given the parameters from the construction scripts), which
        additionally can also generate points for <a href="#ISobol">interlaced
        Sobol' sequences (ISobol)</a>. These generators have their own web page called the
        <a href="https://people.cs.kuleuven.be/~dirk.nuyens/qmc-generators/">Magic Point Shop</a>.</li>
</ol>
<p>
The current version of the QMC4PDE package is dated <strong>10 Dec 2024</strong>.
</p>
<div class="alert alert-info">
  <strong>Download the code
      <a href="qmc4pde-20241210.tgz"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-download"></span> qmc4pde-20231012.tgz</button></a> as a package.</strong>
  <br/>
  To be able to construct polynomial lattice rules you need the following data files: 
  <a href="qmc4pde/polylat-data.tgz">polylat-data.tgz</a> (61 MB). This file has to be
  unpacked in the same directory as where the construction scripts reside (qmc4pde). In a
  future update we will provide the C++ code to generate these data files
  (possibly on the fly).
  <br/>
  Alternatively you may find the Python codes online as the <a
    href="https://bitbucket.org/dnuyens/qmc4pde"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-folder-open"></span> qmc4pde git repository</button></a> on
  bitbucket.
  <br/>
  To generate the points please download the generators from the 
  <a href="https://people.cs.kuleuven.be/~dirk.nuyens/qmc-generators/">Magic Point Shop</a>.
  There is choice of Python2/3, Matlab/Octave and C++ point generators.
  <br/>
  We also have a set of prepackaged interlaced Sobol' points for use with the
  point generators below, available for download: 
  <a href="../qmc-generators/sobolmats.tgz">sobolmats.tgz</a> (19 MB).
</div>
<p>
The Python scripts make use of the <a href="http://www.numpy.org">NumPy
Python package</a> (which is probably included in your Python distribution) as
well as of the <a href="http://www.scipy.org">SciPy Python package</a> in the
case of lattice rules where the Riemann $\zeta$ function and erf functions are needed.
</p>

<p>
For integration over $\bbR^s$ (i.e., the lognormal setting for the PDE) we are currently
investigating alternative choices to control the derivatives in the norm.
A future update will likely include this alternative setting.
</p>

<p>Change history:</p>
<ol>
    <li>20241210: update documentation (webpage) to include explicit description of weights.</li>
    <li>20231012: fix deprecated <tt>np.float</tt> and <tt>collections.Callable</tt> thanks to Bill McLean.</li>
    <li>20180616: forced another integer division thanks to Pierre Marion.</li>
    <li>20171214: Added link to bitbucket git repository. Also, from now on the
      MagicPointShop qmc-generators are in a separate package which makes it easier to maintain.</li>
    <li>20170908: forced another integer division for numpy.tile &gt;= 1.13 (or 1.12?), thanks to Benjamin Raffin</li>
    <li>20170907: modified cbc scripts such that they can be used with python 2 or python 3; 
        changed directory layout; 
        updated Python generator latticeseq_b2.py to generate in blocks of
        powers of 2 to gain significand speed increase when no intermediate
        results are needed;
        restructured directory layout (construction scripts are now in a subdirectory qmc4pde/qmc4pde)</li>
    <li>20160814: initial version</li>
</ol>

<h3>1. Fast CBC constructions</h3>
<p>
The construction
algorithms are all fast component-by-component constructions, using
results from <span class="ref">[<a href="#NC06a">NC06a</a>,<a href="#NC06b">NC06b</a>,<a href="#CKN06">CKN06</a>,<a href="#DKLNS14">DKLNS14</a>]</span> and <span class="ref">[<a href="#Nuy14">Nuy14</a>]</span>. For
lattice rules the construction algorithm will output the generating vector
and for the interlaced polynomial lattice rules the algorithm will output
the generating matrices. These generating vectors and matrices can then be
used in the provided point generators.
</p>

<h4>1.1. Randomly shifted lattice rules/sequences</h4>
<p>
For randomly shifted lattice rules the script constructs a good generating
vector $\bsz$. The construction script expects the prime $p$ (which
defaults to $2$) and the power $m$ to be given such that the (maximum)
number of points is $n=p^m$. The generating vector can be used for any
intermediate power of $p$ as an embedded sequence of lattice rules. A
construction of such lattice sequences was proposed in <span class="ref">[<a href="#CKN06">CKN06</a>]</span> and
this is the approach followed in the lattice rule construction code.
</p>

<h4>1.2. Interlaced polynomial lattice rules</h4>
<p>
For interlaced polynomial lattice rules the script constructs the
associated generating matrices. We fix the base of the finite field to
be $2$ for practicalities in the construction, and in the generation of
the points. The number of points is $n = 2^m$, but, in contrast to the
lattice rules, we currently do not provide these as embedded sequences.
The construction script will automatically choose a default irreducible
polynomial of degree $m$ as the modulus polynomial (which can be
overridden by the user). The specific choice of modulus polynomial does
not influence the theoretical error bound. The output of the script will
be both the generating matrices $C_1,\ldots,C_{\alpha s} \in \bbZ_2^{m
\times m}$ of the polynomial lattice rule as well as the generating
matrices $B_1,\ldots,B_s \in \bbZ_2^{\alpha m \times m}$ of the interlaced
polynomial lattice rule.
</p>

<h3>2. Point generators</h3>
<p>
Once the generating vector or the generating matrices have been
constructed, they are used as input to the corresponding point generator.
These point generators are relatively straightforward to program, and
their computational cost to generate a point is really minor and so can be
neglected for practical purposes; in fact they are comparable to the
fastest random number generators. 
To generate lattice points, one only requires an integer multiplication, a
modulus operation, and a fixed float multiplication/division per
dimension. This is comparable to the cost of a simple
LCG (linear congruential generator) per dimension.
To generate (interlaced) polynomial lattice points in Gray code ordering,
one needs an xor instruction and a fixed float
multiplication/division per dimension. Additionally, a CTZ (count trailing zeros)
algorithm is used to determine the column number of the generating
matrices to perform each xor instruction, which is available on
most CPUs as a machine instruction or can be implemented with a simple
algorithm having a fixed low arithmetic cost. This is comparable to the
cost of a LFSR (linear feedback shift register) generator per dimension.

In the case of randomly shifted lattice
rules the points still have to be randomly shifted before being used as
quadrature points. As QMC points are naturally enumerated, it is
straightforward to parallelize the solving of the different PDE problems
and we therefore equip the point generators with an option to start at any
offset in the enumeration of the points.
</p>

<h3>3. Parameters for the construction scripts</h3>
<p>
The theory in the article <span class="ref">[<a href="#KN16">KN16</a>]</span> 
covers single-level and multi-level algorithms
for both the uniform and lognormal cases, and in the uniform case it
considers both first order (randomly shifted lattice rules) and higher
order methods (interlaced polynomial lattice rules). As the theory is often
quite involved, we extract the essential properties of the analysis in the next
section and allow them to be applied to any general integrand which shares
similar characteristics.
The table in the next section clarifies all parameters.
</p>
  </div>

  <div id="Bound" class="container-fluid section">
      <h1>SPOD bounds on the mixed partial derrivatives</h1>

<p>
We consider a linear functional $G$ applied to $u^s_h(\bsx,\bsy)$ &mdash; the
approximate solution of the PDE, given a specific random instance.
The randomness is parametrized in the form of an infinite vector $\bsy$ which 
is truncated to $s$ dimensions in the computational approach.
Taking $F(\bsy) = G(u_h^s(\cdot,\bsy))$ we approximate the expected
value of this function through a QMC rule.
The simplified view is thus the integration of an infinite-dimensional function,
over $\bsy$ against some distribution, truncated to $s$ dimensions. This
$s$-dimensional function is denoted by $F(\bsy)$.
</p>
<p>
We distinguish two cases:
<div class="row">
    <div class="col-md-6">
        <strong>1. Uniform case:</strong> Integrate against the uniform measure<br/>
\begin{align*}
  \mathbb{E}[G(u(\cdot,\bsy))]
  &amp;=
  \int_{[-1/2,1/2]^\bbN} G(u(\cdot,\bsy)) \rd{\bsy}
  \\
  &amp;\approx
  \int_{[-1/2,1/2]^s} G(u_h^s(\cdot,\bsy)) \rd{\bsy}
  \\
  &amp;=
  \int_{[-1/2,1/2]^s} F(\bsy) \rd{\bsy}
  \\
  &amp;\approx
  \frac{1}{n} \sum_{k=0}^{n-1} F(\bst_k)
  .
\end{align*}
    </div>
    <div class="col-md-6">
        <strong>2. Normal/lognormal case:</strong> Integrate against the normal distribution $\phi(\bsy)$<br/>
\begin{align*}
  \mathbb{E}[G(u(\cdot,\bsy))]
  &amp;=
  \int_{\bbR^\bbN} G(u(\cdot,\bsy)) \, \phi(\bsy) \rd{\bsy}
  \\
  &amp;\approx
  \int_{\bbR^s} G(u_h^s(\cdot,\bsy)) \, \phi(\bsy) \rd{\bsy}
  \\
  &amp;=
  \int_{\bbR^s} F(\bsy) \, \phi(\bsy) \rd{\bsy}
  \\
  &amp;\approx
  \frac{1}{n} \sum_{k=0}^{n-1} F(\Phi^{-1}(\bst_k))
  .
\end{align*}
    </div>
</div>
</p>
<p>
The integral is thus either against the uniform distribution on $[-1/2,1/2]^s$ or
against the standard Gaussian distribution on $\bbR^s$. 
The latter situation arises when generating lognormal (and normal) random
fields and is thus referred to as the lognormal case. For the lognormal case the only 
theoretical results are by means of randomly shifted lattice rules (RLR) which 
then give convergence close to $O(n^{-1})$ in approximating the integral.
</p>
<p>
For the uniform case we can apply randomly shifted lattice rules (RLR), obtaining a
convergence close to $O(n^{-1})$, or interlaced polynomial lattice rules (IPLR),
obtaining a convergence close to $O(n^{-\alpha})$ for an interlacing factor of
$\alpha$.  For interlaced polynomial lattice rules this interlacing factor must be
an integer greater than 1. Theoretically $\alpha$ can be as big as one wants,
but we are limited by the precision of the float representation as we need $m
\times \alpha$ bits to represent the points if we want $n = 2^m$. Therefore the
most realistic values of $\alpha$ are probably 2 and 3.
</p>

<p>
We provide two Python scripts, <code>lat-cbc.py</code> (see section <a
href="#RLR">RLR</a>)  and <code>polylat-cbc.py</code> (see section <a
href="#IPLR">IPLR</a>) (which are in fact symbolic links to
<code>spod-cbc.py</code>), to construct lattice rules and interlaced polynomial
lattice rules, respectively, in which the following generalized SPOD
(smoothness driven product and order dependent) bound on
the mixed derivatives of the integrand is assumed: for all $\bsnu \in \{0,
..., \alpha\}^s$,
\begin{align}\label{eq:general-bound}\tag{1}
\left| \partial^\bsnu F (\bsy) \right|
\,\lesssim\,
 \Big( (|\bsnu|+a_1)! \Big)^{d_1}
 \prod_{j=1}^s (a_2 \, \Bj)^{\nu_j}\,
                \exp(a_3\,\Bj|y_j|)
\,,
\end{align}
for some integers $\alpha \ge 1$ and $a_1 \ge 0$, real numbers $a_2 \gt 0$,
$a_3 \ge 0$ and $d_1 \ge 0$, and a sequence of positive numbers $\Bj$,
corresponding to the values of $b_j$, $\overline{b}_j$, $\beta_j$ or
$\overline{\beta}_j$, appropriate for the setting, see the article for
full details. For single-level algorithms the integrand is $F (\bsy) =
G(u_h^s(\cdot,\bsy))$, while for multi-level algorithms the integrand at
level $\ell$ is $F(\bsy) =
G((u_{h_\ell}^s-u_{h_{\ell-1}}^s)(\cdot,\bsy))$.
These parameters will then result into the following weights $\gamma_\setu$:
<div class="row bg-info border border-2">
    <div class="col-md-6">
      <strong>a. RLR (only order $\alpha = 1$ in the context of this software currently):</strong></br>
\begin{align*}
  \gamma_\setu
  &amp;=
  \Big( \big( ( |\setu| + a_1 )! \big)^{d_1} \prod_{j \in \setu} \frac{a_2 \, b_j}{\sqrt{\rho(\lambda)}} \Big)^{2/(1+\lambda)}
\end{align*}
with $\rho(\lambda) = 2\zeta(2\lambda) / (2\pi^2)^\lambda$ and $\lambda = 1 /
\min(2-2\delta, 2d_2-1) \in (1/2, 1]$ for a decay $d_2 \in \{2,3,\ldots\}$ of the $b_j$
and a choice of $\delta \in (0, 1/2)$. The default choice for $\delta = 0.125$
and for $d_2 = 2$ (i.e., $b_j = c\,j^{-d_2}$).
The convergence rate then is $n^{-1/(2\lambda)}$.
    </div>
    <div class="col-md-6">
      <strong>b. IPLR (with interlacing factor $\alpha \in \{2,3,\ldots\}$, only uniform case currently):</strong></br>
\begin{align*}
  \gamma_\setu
  &amp;=
  \sum_{\bsnu_\setu \in \{1,\ldots,\alpha\}^{|\setu|}} \big((|\bsnu|_\setu+a_1)!\big)^{d_1} \, \prod_{j\in\setu} 2^{\delta(\nu_j,\alpha)} \, (a_2 \, b_j)^{\nu_j}
\end{align*}
with $\lambda \in (1/\min(\alpha, d_2), 1]$.
The convergence rate then is $n^{-1/\lambda}$.
    </div>
</div>
</p>
<p>
An overview of all parameters and their description for the
<code>lat-cbc.py</code> and the 
<code>polylat-cbc.py</code> scripts is given in
the following table and we give examples of usage in the next two sections.
</p>

<table class="table-bordered table-condensed">
<tr><td><code>s</code></td><td></td><td>number of dimensions</td></tr>
<tr><td><code>m</code></td><td></td><td>number of points given by $2^m$
        (or $p^m$ in case of optional argument <code>p</code> for lattice rules)</td></tr>
<tr><td><code>p</code></td><td>optional</td><td>
        <ul style="margin-left: 0.5em; padding-left: 1em;">
                <li>for lattice rules $n=p^m$, prime, defaults to $p=2$</li>
                <li>for polynomial lattice rules this is the
                primitive modulus polynomial of degree $m$
                (the code uses a table of default polynomials)</li>
    </ul></td></tr>
    <tr><td><code>alpha</code></td><td>optional</td><td>
            <ul style="margin-left: 0.5em; padding-left: 1em;">
                <li>no effect for lattice rules, $\alpha = 1$</li>
                <li>integer interlacing factor for polynomial lattice rules, $\alpha \ge 2$
                (defaults to $\alpha=2$)</li>
    </ul></td></tr>
    <tr><td><code>a1</code></td><td>optional,<br/> defaults to&nbsp;0</td><td>integer offset for factorial, <br/>
            e.g., in our analysis, $a_1=0$ for single-level and
            $a_1=5$ for multi-level</td></tr>
    <tr><td><code>a2</code></td><td>optional,<br/> defaults to&nbsp;1</td><td>scaling in the product (can be Python expression) <br/>
            e.g., in our analysis, $a_2=1$ for uniform and
            $a_2=1/\ln 2$ for single-level lognormal and
            $a_2=2$ for multi-level lognormal</td></tr>
    <tr><td><code>a3</code></td><td>optional,<br/> defaults to&nbsp;0</td><td>boundary behaviour for the lognormal case
            ($a_3=0$ means we are in the uniform case) <br/>
            e.g., in our analysis, $a_3=1$ for single-level lognormal and 
            $a_3=9$ for multi-level lognormal</td></tr>
    <tr><td><code>d1</code></td><td>optional,<br/> defaults to&nbsp;1</td><td>extra power on the factorial factor
            ($d_1=0$ implies product weights)</td></tr>
    <tr><td><code>d2</code></td><td>optional,<br/> defaults to&nbsp;2</td><td>decay of the $\Bj$ sequence, $d_2 \gt 1$</td></tr>
    <tr><td><code>b</code></td><td>optional,<br/> defaults to $cj^{-d_2}$</td><td>the $\Bj$ sequence as a Python expression, see text
            (alternatively as numerical values through a file with <code>b_file</code>)</td></tr>
    <tr><td><code>c</code></td><td>optional,<br/> defaults to&nbsp;1</td><td>in case no <code>b</code> and <code>b_file</code> are given, $\Bj$ is set to $cj^{-d_2}$</td></tr>
    <tr><td><code>b_file</code></td><td>optional</td><td>file name containing numerical values for the sequence $\Bj$</td></tr>
    <tr><td><code>out</code></td><td>optional</td><td>output directory to write results to</td></tr>
</table>

<p style="margin-top: 1em;">
The summary of bounds from the analysis of the article corresponds to
taking $d_1 = 1$. For randomly shifted lattice rules the order of
convergence is limited to 1 and therefore $\alpha = 1$. For interlaced
polynomial lattice rules we need $\alpha \ge 2$. The uniform case
corresponds to taking $a_3 = 0$. The lognormal case corresponds to taking
$a_3 \gt 0$, with $a_3=1$ and $a_3=9$ for the single-level and multi-level
algorithms, respectively. Our analysis lead to $a_1=0$ and $a_1=5$ for the
single-level and multi-level algorithms, respectively.
<!--
(Following the proof arguments in \cite{KSS15,DKLS} we could set $a_1=3$
for the multi-level algorithms in the uniform case, but the sequence $\Bj$
is defined differently.)
-->
We have $a_2 = 1$ in the uniform case, while in the lognormal case we have
$a_2=1/\ln 2$ and $a_2=2$ for the single-level and multi-level algorithms,
respectively. To cater for other potential integrands which satisfy the
generalized bound \eqref{eq:general-bound}, our scripts can take general
values of $a_1$, $a_2$, $a_3$ and $d_1$ as input.
</p>


<p>
To specify the sequence $\Bj$ the user has two main options. Either the
user provides a Python expression (with access to variables <code>j</code> and
<code>v</code>, to stand for the values of $j$ and $\nu_j$) as the argument to
command line option <code>b</code>, or the user provides the name of an input
file containing numerical values for each of the $\Bj$ by means of the
<code>b_file</code> option. (Other possibilities are available, including
a configuration file, but are not discussed here for conciseness.)
</p>

<p>
We remark that the analysis in the article takes into account the
truncation from infinite dimensions to $s$ dimensions. Therefore it is
essential to have an idea of the summability of the infinite
sequence $\Bj$. For this, take $p_* \in
(0,1)$ for which $\sum_{j=1}^\infty \Bj^{\;\,{p_*}} \lt \infty$. We would
like $p_*$ to be as small as possible to obtain the fastest convergence of the
integral. Asit is more convenient to work with the reciprocal value, the script
expects a value $d_2 \gt 1$. This is called the &ldquo;decay&rdquo; of the
sequence $\Bj$.
</p>

<p>
The theoretical QMC convergence rate in the context of PDE problems, with
the implied constant independent of $s$, is roughly of order
$n^{-\min(1,d_2-1/2)}$ for randomly shifted lattice rules, and
$n^{-\min(\alpha,d_2)}$ for interlaced polynomial lattice rules with
interlacing factor $\alpha\ge 2$.
</p>

<p>
The analysis in the article can be extended to handle the
bound \eqref{eq:general-bound} 
with a general exponent $d_1$ on the
factorial factor, provided that $d_2 \gt d_1$ (to ensure that the implied
constant in the error estimate is bounded independently of $s$). The case
$d_1 = 0$ will lead to product weights $\gamma_\setu$, in which case the
CBC construction could be performed at lower cost.
</p>
  </div>

  <div id="RLR" class="container-fluid section">
      <h1>Randomly shifted lattice rules</h1>
<p>
A synopsis of how to call the Python script <code>lat-cbc.py</code> to construct
a randomly shifted lattice rule with $n=2^m$ points is
</p>

<pre><code>./lat-cbc.py --s={s} --m={m} [--a1={a1}] [--a2={a2}] [--a3={a3}] --d2={d2}
             [--b="{bound-function}" | --b_file={file_name} | --c={c}]
</code></pre>

<p>
In particular, the default value of $a_3 = 0$ specifies the
uniform case, while any value of $a_3 \gt 0$ specifies the lognormal case.
</p>

<p>
We give some examples on how to call the script:
</p>

<pre><code>## uniform case, 100-dimensional rule, 2^10 points and with specified bounds b:
./lat-cbc.py --s=100 --m=10 --d2=3 --b="0.1 * j**-3 / log(j+1)"
</code></pre>

<pre><code>## as above, but multi-level and with bounds from file:
./lat-cbc.py --s=100 --m=10 --a1=5 --d2=3 --b_file=bounds.txt
</code></pre>

<pre><code>## lognormal case, 100-dimensional rule, 2^10 points and with algebraic decay:
./lat-cbc.py --s=100 --m=10 --a2="1/log(2)" --a3=1 --d2=3 --c=0.1
</code></pre>

<pre><code>## as above, but multi-level and with bounds from file:
./lat-cbc.py --s=100 --m=10 --a1=5 --a2=2 --a3=9 --d2=3 --b_file=bounds.txt
</code></pre>

<p>
This will produce several files in the output directory. The most
important one is <code>z.txt</code> which contains the generating vector. These
points then need to be randomly shifted for the theory to apply. In the
lognormal case, the randomly shifted points should be mapped to
$\bbR^s$ by applying the inverse of the cumulative normal distribution
function component-wise.
</p>

<p>
Codes are available in Python 
<a href="../qmc-generators/python/latticeseq_b2.py"><code>latticeseq_b2.py</code></a>, Matlab/Octave 
<a href="../qmc-generators/matlab/latticeseq_b2.m"><code>latticeseq_b2.m</code></a>, and C++ 
<a href="../qmc-generators/cpp/latticeseq_b2.hpp"><code>latticeseq_b2.hpp</code></a> and 
<a href="../qmc-generators/cpp/latticeseq_b2.cpp"><code>latticeseq_b2.cpp</code></a>
to generate lattice points. An example usage in Matlab is given below:
</p>

<pre><code>load z.txt                      % load generating vector
latticeseq_b2('init0', z)       % initialize the procedural generator
Pa = latticeseq_b2(20, 512);    % first 512 20-dimensional points
Pb = latticeseq_b2(20, 512);    % next 512 20-dimensional points
</code></pre>

<p>
With respect to the multi-level algorithm there are two important features
of these lattice rules: they are lattice sequences in terms of the number
of points, and they are constructed by a component-by-component
algorithm which allows a rule constructed for $s$ dimensions to be used
for a lower number of dimensions. This means the construction only has to
be done once for the maximum number of points $\max_{0\le\ell\le L}
n_\ell$ and the maximum number of dimensions $s_L$, since the parameters
in \eqref{eq:general-bound} 
are the same for all levels.
</p>
  </div>

  <div id="IPLR" class="container-fluid section">
      <h1>Interlaced polynomial lattice rules</h1>
<p>
A synopsis of how to call the Python script <code>polylat-cbc.py</code> to
construct an interlaced polynomial lattice rule with $n=2^m$ and
interlacing factor $\alpha\ge 2$ is
</p>

<pre><code>./polylat-cbc.py --s={s} --m={m} --alpha={alpha} [--a1={a1}] [--a2={a2}]
             [--b="{bound-function}" | --b_file={file_name} | --d2={d2} --c={c}]
</code></pre>

<p>
We expect the theoretical convergence rate to be of order
close to $n^{-\min(\alpha,d_2)}$.
</p>

<p>
We give some examples on how to call the script:
</p>

<pre><code>## 100-dimensional rule, 2^10 points, interlacing 3 and with specified bounds b:
./polylat-cbc.py --s=100 --m=10 --alpha=3 --b="0.1 * j**-3 / log(j+1)"
</code></pre>

<pre><code>## as above, but multi-level and with bounds from file:
./polylat-cbc.py --s=100 --m=10 --alpha=3 --a1=5 --b_file=bounds.txt
</code></pre>

<p>
Several files will be saved into the output directory.
The most important one is <code>Bs.col</code> which contains the
generating matrices of the interlaced polynomial lattice rule.
(Also the generating matrices of the non-interlaced polynomial
lattice rule are available in the file <code>Cs.col</code>.)
</p>

<p>
Codes are available in Python 
<a href="../qmc-generators/python/digitalseq_b2g.py"><code>digitalseq_b2g.py</code></a>, Matlab/Octave 
<a href="../qmc-generators/matlab/digitalseq_b2g.m"><code>digitalseq_b2g.m</code></a>, and C++ 
<a href="../qmc-generators/cpp/digitalseq_b2g.hpp"><code>digitalseq_b2g.hpp</code></a> and 
<a href="../qmc-generators/cpp/digitalseq_b2g.cpp"><code>digitalseq_b2g.cpp</code></a> 
to generate these points. 
For interlacing to work correctly, the product $\alpha m$ should
be no more than the number of available bits. We note that the IEEE double
precision type only has 53 bits available and Matlab uses this type to do
its calculations. As a compromise (which comes with no guarantee) we load
the generating matrices truncated to 53 bits precision, which are
available in the file <code>Bs53.col</code>. Similarly, to cater for the
extended long double precision in C++ we provide a file <code>Bs64.col</code>.
For instance, with interlacing factor $4$ we can have up to $2^{13}$
points in Matlab and up to $2^{16}$ points in C++ using long double. The
Python point generator is implemented such that it can use arbitrary
precision. One can also change the C++ generator to use arbitrary
precision.
</p>

<p>
Below we give an example to illustrate how to feed the output from the
construction script into the point generator. At the same time we
experiment on the effect of truncating the generating matrices. First
construct the generating matrices using the Python script and then save
these points (in long double precision) to the file <code>points.txt</code>
using the C++ example program:
</p>

<pre><code>./polylat-cbc.py --s=10 --m=15 --alpha=4 --b="0.1 * j**-4" --out=.
./digitalseq_b2g &lt;Bs64.col &gt;points.txt
</code></pre>

<p>
In this example of $2^{15}$ points with interlacing factor $4$,
we need $4\times 15 = 60$ bits of precision, which can be realized in full
in C++ using long double. Now we use the generating matrices
(truncated to 53 bits) in Matlab, make a plot, and compare these points to the
full precision points we just created on the command line:
</p>

<pre><code>load Bs53.col                      % load generating matrices
s = 10; m = 15;
digitalseq_b2g('init0', Bs53)      % initialize the procedural generator
Pa = digitalseq_b2g(s, pow2(m-1)); % first half of the points
Pb = digitalseq_b2g(s, pow2(m-1)); % second half of the points
s1 = 2; s2 = 10;                   % pick some dimensions to show
plot(Pa(s1,:), Pa(s2,:), 'b.', Pb(s1,:), Pb(s2,:), 'r.')
axis([0 1 0 1]); axis square

load points.txt  % compare with the C++ generated points in long double
points = points';
Pc = points(:,1:pow2(m-1));
Pd = points(:,pow2(m-1)+(1:pow2(m-1)));
norm(Pc - Pa)    % this should be in the order of 1e-14
norm(Pd - Pb)    % and this as well...
</code></pre>

<p>
Here the effect of truncating the generating matrices appears to
be empirically insignificant, but the higher order QMC convergence
theory no longer applies and there is no guarantee how well they would
perform in practice.
</p>
  </div>

  <div id="ISobol" class="container-fluid section">
      <h1>Interlaced Sobol' points</h1>
<p>
We note that since the point generators operate using generating matrices,
they can be used to generate any other digital sequence, interlaced or
not. We provide the generating matrices for an
implementation of the Sobol' sequence from 
<span class="ref">[<a href="#JK08">JK08</a>]</span> with 21201 dimensions (as the file
<code>sobol_Cs.col</code>), as well as the generating matrices for interlaced
Sobol' sequences with interlacing factor $\alpha = 2,3,4,5$ (e.g.,
<code>sobol_alpha3_Bs53.col</code>). An example usage in Matlab to generate the
points is given below:
</p>

<pre><code>load sobol_alpha3_Bs53.col                 % load generating matrices
digitalseq_b2g('init0', sobol_alpha3_Bs53) % initialize the procedural generator
Pa = digitalseq_b2g(10, 1024);             % first 1024 10-dimensional points
Pb = digitalseq_b2g(10, 1024);             % next 1024 10-dimensional points
</code></pre>



  </div>

  <div id="References" class="container-fluid section">
      <h1>Further resources</h1>
      <ul>
          <li><a href="https://people.cs.kuleuven.be/~dirk.nuyens/qmc-generators/">Magic Point Shop by D. Nuyens</a></li>
          <li><a href="https://people.cs.kuleuven.be/~dirk.nuyens/fast-cbc/">Fast CBC construction codes in Matlab by D. Nuyens</a></li>
          <li><a href="http://web.maths.unsw.edu.au/~fkuo/sobol/">Sobol sequence generator on F. Y. Kuo website</a></li>
          <li><a href="https://bitbucket.org/dnuyens/qmc4pde">qmc4pde git repository</a></li>
          <li><a href="https://bitbucket.org/dnuyens/qmc-generators">qmc-generators git repository</a></li>
      </ul>
      <h1>References</h1>
<dl class="dl-horizontal">
    <dt id="CKN06">[CKN06]</dt>
    <dd>R. Cools, F. Y. Kuo, and D. Nuyens,
    Constructing embedded lattice rules for multivariate integration
    SIAM J. Sci. Comput., 28 (2006), 2162 - 2188.
    </dd>

    <dt id="DKLNS14">[DKLNS14]</dt>
    <dd>
    J. Dick, F. Y. Kuo, Q. T. Le Gia, D. Nuyens, and Ch. Schwab,
    Higher order QMC Galerkin discretization for parametric operator equations,
    SIAM J. Numer. Anal., 52 (2014), 2676 - 2702.
    </dd>

    <dt id="JK08">[JK08]</dt>
    <dd>
    S. Joe and F. Y. Kuo,
    Constructing Sobol' sequences with better two-dimensional projections,
    SIAM J. Sci. Comput., 30 (2008), 2635 - 2654.
    </dd>

    <dt id="KN16">[KN16]</dt>
    <dd>
    F. Y. Kuo and D. Nuyens,
    Application of quasi-Monte Carlo methods to elliptic PDEs with random
    diffusion coefficients &mdash; a survey of analysis and implementation,
    Foundations of Computational Mathematics, (2016).

    </dd>

    <dt id="Nuy14">[Nuy14]</dt>
    <dd>
    D. Nuyens,
    The construction of good lattice rules and polynomial lattice rules,
    In: Uniform Distribution and Quasi-Monte Carlo Methods
    (P. Kritzer, H. Niederreiter, F. Pillichshammer, A. Winterhof, eds.),
    Radon Series on Computational and Applied Mathematics Vol. 15,
    De Gruyter, 2014, pp. 223 - 256.
    </dd>

    <dt id="NC06a">[NC06a]</dt>
    <dd>
    D. Nuyens and R. Cools,
    Fast algorithms for component-by-component construction of
    rank-1 lattice rules in shift-invariant reproducing kernel
    Hilbert spaces,
    Math. Comp., 75 (2006), 903 - 920.
    </dd>

    <dt id="NC06b">[NC06b]</dt>
    <dd>
    D. Nuyens and R. Cools,
    Fast component-by-component construction of rank-1 lattice rules with
    a non-prime number of points,
    J. Complexity, 22 (2006), 4 - 28.
    </dd>
</dl>
  </div>

  <footer class="container-fluid">
    <p>&copy; 2016, ..., 2023 Dirk Nuyens (KU Leuven, Belgium) and Frances Y. Kuo (UNSW, Australia).</p> 
  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  <script type="text/javascript">
  /* <![CDATA[ */
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-105294-3']);
  _gaq.push(['_trackPageview']);
  (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   /* ]]> */
 </script> 
  </body>
</html>
