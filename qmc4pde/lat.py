
####
## (C) Dirk Nuyens, KU Leuven, 2008,...,2016,2017,2024,...
## See https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/

from __future__ import print_function
import numpy as np
import os

try:
    from collections.abc import Callable
except ImportError:
    from collections import Callable

# this is currently kind of a hack
try: from __main__ import MP
except: MP = False
# mp only used currently for calculating weight functions, not for fast CBC
if MP:
    from mpmath import zeta, pi, exp, sqrt, erf, erfc, erfinv, expm1, inf
    print("MP", MP) 
else:
    from numpy import pi, exp, sqrt, expm1, inf
    from scipy.special import zeta, erf, erfc, erfinv

def B2(t):
    return t**2 - t + (t-t+1)/6. # t-t+1 is 1 in the type of t

def fastrank1expod_base2(s_max, m2, gamma, Gamma_r, omega=B2, omega_cst=0, m1=None, qstar=None, 
        float_t=float, float_dtype=float, CBC_optimal=None, verbose=2, log=None):
    """
      Construct rank-1 lattice rule or sequence for n=2**m2 with POD weights:
         \gamma_\setu = \Gamma_{|\setu|} \prod_{j \in \setu} \gamma_j
      where \Gamma and \gamma are given as functions (or as a list).
      In fact, the argument specifies the ratio of consecutive \Gamma_\ell:
         \Gamma_^{\mathrm{ratio}}{\ell} = \Gamma_\ell / \Gamma_{\ell-1}
      with \Gamma_0 = 1.

      The kernel of the reproducing kernel Hilbert space is
         K_s(x, y) = \sum_{\setu \subseteq \{1:s\}} \gamma_\setu 
                             \prod_{j \in \setu} \omega(\{x_j-y_j\})
      and we assume \int_0^1 \omega(x) dx = 0, \omega(x) = \omega(1-x) and
      \gamma_\emptyset = 1 in the construction code.

      Inputs:
        s_max           number of dimensions
        m2              maximum number of points 2**m2, m2 >= 1, reasonable
                        choices would be m2 <= 25 (but this will take a longer
                        time; m2 = 20 for approximate a million points and that
                        should run fast)
        gamma           product part weight, a lambda function which gets the
                        dimension as argument
        Gamma_r         order-dependent part of the weight as ratio of
                        consecutive dimensions (\Gamma_\ell / \Gamma_{\ell-1},
                        \Gamma_0 = 1), a lambda function which gets the \ell =
                        |\setu| as argument, will not be evaluated in 0 (the
                        code just assumes this would be 1)
        omega           variable part of the kernel as lambda function,
                        could be a list of functions, one per dimension (for \R^s)
        omega_cst       if omega has a constant part it could be put here, e.g.,
                        in case of the kernel over \R^s this is useful, could
                        be a list of constant parts
        m1              minimum number of points to consider when constructing
                        extensible rule, m1 >= 1

      Outputs:
        z               generating vector mod 2**m2
        e2              squared worst-case error per dimension from 1 upto s_max
        x               either the worst-case error per dimension or the
                        X-factor (how much worse than optimal) in case of
                        extensible rule
        e2s             for each nb of dimensions the squared worst-case for
                        n=2**0, ..., 2**m2
        CBC_optimal     in case of extensible construction: dictionary with per
                        power m = m1,...,m2 the squared worst-case error per
                        dimension for 2**m when constructed as a non-extensible
                        rule (i.e., the benchmark)

      Example (and doctest test):

      >>> s = 10; m2 = 10; m1 = 10
      >>> gamma = lambda j : j**-2; Gamma_r = lambda ell : ell # = factorial(ell) / factorial(ell-1)
      >>> (z, e2, x, e2s) = fastrank1expod_base2(s, m2, gamma, Gamma_r, m1=m1)[:4]
      Calculating optimal CBC for 2**10
      s=   1, z=     1, e2=1.5895e-07, x=3.9868e-04, mloc=  10
      s=   2, z=   275, e2=4.4153e-07, x=6.6448e-04, mloc=  10
      s=   3, z=   179, e2=7.6664e-07, x=8.7558e-04, mloc=  10
      s=   4, z=   319, e2=1.0712e-06, x=1.0350e-03, mloc=  10
      s=   5, z=   109, e2=1.3719e-06, x=1.1713e-03, mloc=  10
      s=   6, z=   299, e2=1.6126e-06, x=1.2699e-03, mloc=  10
      s=   7, z=   491, e2=1.8289e-06, x=1.3524e-03, mloc=  10
      s=   8, z=   143, e2=2.0112e-06, x=1.4182e-03, mloc=  10
      s=   9, z=    81, e2=2.1631e-06, x=1.4708e-03, mloc=  10
      s=  10, z=   395, e2=2.2982e-06, x=1.5160e-03, mloc=  10

      >>> s = 10; m2 = 10; m1 = 10; lambda_ = 0.55; alpha = 1/4.
      >>> gamma = lambda j: (j**-2)**(1/(1+lambda_)); Gamma_r = lambda ell: ell**(2/(1+lambda_)) # ie Gamma(ell) = factorial(ell)^(2/(1+lambda_))
      >>> from lat import theta_ab, theta_c
      >>> omega = lambda x: theta_ab(x, alpha); omega_cst = theta_c(alpha)
      >>> (z, e2, x, e2s) = fastrank1expod_base2(s, m2, gamma, Gamma_r, omega, omega_cst, m1=1)[:4]
      Calculating optimal CBC for 2**1
      Calculating optimal CBC for 2**2
      Calculating optimal CBC for 2**3
      Calculating optimal CBC for 2**4
      Calculating optimal CBC for 2**5
      Calculating optimal CBC for 2**6
      Calculating optimal CBC for 2**7
      Calculating optimal CBC for 2**8
      Calculating optimal CBC for 2**9
      Calculating optimal CBC for 2**10
      Calculating rule for 2**1 to 2**10
      s=   1, z=     1, e2=3.8215e-06, x=1.0000e+00, mloc=   1
      s=   2, z=   155, e2=7.0023e-05, x=1.1323e+00, mloc=   8
      s=   3, z=   477, e2=4.4279e-04, x=1.1224e+00, mloc=  10
      s=   4, z=    45, e2=1.3079e-03, x=1.0629e+00, mloc=   9
      s=   5, z=   397, e2=3.0372e-03, x=1.0400e+00, mloc=  10
      s=   6, z=   207, e2=5.8355e-03, x=1.0227e+00, mloc=   9
      s=   7, z=   285, e2=9.8834e-03, x=1.0148e+00, mloc=   7
      s=   8, z=   297, e2=1.5929e-02, x=1.0111e+00, mloc=   7
      s=   9, z=   347, e2=2.4471e-02, x=1.0098e+00, mloc=  10
      s=  10, z=   487, e2=3.6339e-02, x=1.0080e+00, mloc=   8

      Note that it is not guaranteed that you get the same generating vector
      when running this algorithm on different machines, in different languages
      or with different FFTs. The worst-case errors at the end should be
      comparable though.
      Wrt the doctest above: it is hoped that you get exactly this result such
      that the doctest will not fail.
      The code was tested against order-dependent and product weights Matlab
      code.

      Note that this script can also construct rules for product weights and
      for order-dependent weights (setting the other part to 1), but the
      construction cost is always that of order-dependent weights which is 
      O(s n \log n + s^2 n). For product weights the s^2 can be avoided by
      using a special product weight construction.

      (C) Dirk Nuyens, KU Leuven, 2016
    """

    from numpy import r_, array, tile, cumsum, arange, all
    # the following need to be adjusted to allow for mp
    from numpy import zeros, ones
    from numpy.fft import fft, ifft
    from numpy import sqrt

    index_t = 'i' # integer representations here are limited to 32 bit (the methods above work for arbitrary length though)

    if not hasattr(omega, '__iter__') and hasattr(omega_cst, '__iter__'): raise ValueError("If omega_cst is a list, then so must be omega itself.")
    if m1 == None: m1 = m2
    if isinstance(gamma, Callable): gamma = [0] + [ gamma(j) for j in range(1, s_max+1) ] # indexable by just j (we have an extra gamma[0])
    if isinstance(Gamma_r, Callable): Gamma_r = [1] + [ Gamma_r(ell) for ell in range(1, s_max+1) ] # idem

    if all(array(Gamma_r) == 1): 
        product_weights = True
        print("product weights detected, using faster algorithm")
    else: 
        product_weights = False
        if qstar: print("qstar = ", qstar)

    if CBC_optimal == None:
        CBC_optimal = {}
        CBC_optimal[m2] = ones(s_max, float_dtype) # this is used when m1 == m2
        if m1 != m2:
            for m in range(m1, m2+1):
                out = fastrank1expod_base2(s_max, m, gamma, Gamma_r, omega, omega_cst, m, qstar, float_t, float_dtype, None, verbose-1)
                CBC_optimal[m] = out[1]

    p = 2 # we only do base 2
    n = p**m2
    g = 5 # pseudo generator for p = 2

    phip = ones(m2+1, index_t)
    r    = ones(m2+1, index_t)
    phip[1] = p-1
    for m in range(2, m2+1): 
        phip[m] = p * phip[m-1]
    r[phip >= 2] = 2
    phip[phip >= 2] //= 2 # symmetry of the kernel function, omega(x) = omega(1-x), is used here

    perm = ones(phip[m2], index_t)
    for i in range(1, phip[m2]): 
        perm[i] = (perm[i-1] * g) % n

    sidx = r_[0, cumsum(phip[:-1])]
    eidx = sidx + phip # end index is non-inclusive

    psi = zeros(sum(phip), float_dtype)
    fft_psi = 0j + psi # zeros(sum(phip), float_dtype) # should be complex version of dtype
    if not hasattr(omega, '__iter__'):
        omegav = np.vectorize(omega)
        for m in range(m2+1):
            psi[sidx[m]:eidx[m]] = omegav( ( perm[:phip[m]] % p**m ) / float_t(p**m) ) + omega_cst
            fft_psi[sidx[m]:eidx[m]] = fft(psi[sidx[m]:eidx[m]])

    if not qstar: qstar = s_max
    if product_weights: qstar = 0
    else: qstar = min(s_max, qstar)

    q = zeros((sum(phip), qstar+1), order='F', dtype=float_dtype) # for product weights we only use index 0 to store the full product
    q[:,0] = 1

    z = zeros(s_max, index_t)
    e2 = zeros(s_max, float_dtype)
    x = zeros(s_max, float_dtype)

    E2 = zeros(phip[m2], float_dtype)
    X = zeros((phip[m2], m2-m1+1), float_dtype)
    S = zeros(sum(phip), float_dtype) # S(k) = sum_{\ell=1}^{s} \Gamma_\ell q_{s-1,\ell-1}(k) 
                                      # or the product \prod_{j=1}^{s-1} (1 + \gamma_j \omega(x_{k,j})) 
                                      # in case of product_weights==True
    prev_e2 = zeros(m2+1, float_dtype) # e2 from previous dimension per power of 2 starting from m=0,...,m2

    if (verbose >= 1) and (m1 != m2):
        print("Calculating rule for %d**%d to %d**%d" % (p, m1, p, m2))
    elif verbose >= 1:
        print("Calculating optimal CBC for %d**%d" % (p, m1))

    e2s = zeros((s_max, m2+1), float_dtype)

    if log: 
        with open(log, 'w') as f:
            f.write("s,z,e2,x,mloc,it_t_min,cur_t_min,rem_t_min\n")

    elapsed_time0 = os.times()[4]

    # below we write i0 for a 0-based index and use i for the 1-based index, i = i0 + 1
    for s0 in range(s_max):
        s = s0 + 1

        elapsed_time1 = os.times()[4]

        if hasattr(omega, '__iter__'):
            if verbose > 1: print("Constructing kernel for j =", s)
            omegav = np.vectorize(omega[s0])
            if hasattr(omega_cst, '__iter__'): omega_cst_ = omega_cst[s0]
            else: omega_cst_ = omega_cst
            for m in range(m2+1):
                psi[sidx[m]:eidx[m]] = omegav( ( perm[:phip[m]] % p**m ) / float_t(p**m) ) + omega_cst_
                fft_psi[sidx[m]:eidx[m]] = fft(psi[sidx[m]:eidx[m]])

        if product_weights:
            S = q[:,0] # alias!
        else:
            S[:] = 0
            for ell in range(1, min(s, qstar)+1):
                S += Gamma_r[ell] * q[:,ell-1]

        E2[:] = 0
        v = 1
        for m in range(m2+1):
            y = ifft( fft_psi[sidx[m]:eidx[m]] * fft(S[sidx[m]:eidx[m]]) )
            E2[:phip[m]] = tile(E2[:v], phip[m]//v) + r[m] * y.real
            if m >= m1:
                X[:,m-m1] = tile( sqrt( (prev_e2[m] + gamma[s] * E2[:phip[m]] / p**m) / CBC_optimal[m][s0] ) , phip[m2]//phip[m] )
            v = phip[m]

        if m1 != m2:
            Xm = X.max(axis=1)
            w0 = Xm.argmin()
        else:
            w0 = E2.argmin() # avoid using the sum when constructing fixed rule
        if s == 1: w0 = 0 # force first component of lattice rule to be 1 (they are all equivalent)

        mloc = X[w0,:].argmax() + m1 # power of m where it happens
        x[s0] = X[w0,mloc-m1]
        z[s0] = perm[w0]
        z[s0] = min(z[s0], n-z[s0]) # pick minimal, they are equivalent
        e2[s0] = prev_e2[m2] + gamma[s] * E2[w0] / p**m2

        if product_weights:
            for m in range(m2+1):
                ww0 = w0 % phip[m]
                J = r_[ww0+sidx[m]:-1+sidx[m]:-1, eidx[m]-1:ww0+sidx[m]:-1]
                q[sidx[m]:eidx[m],0] *= (1 + psi[J] * gamma[s])
        else:
            for ell in range(min(qstar, s+1), 0, -1):
                for m in range(m2+1):
                    ww0 = w0 % phip[m]
                    J = r_[ww0+sidx[m]:-1+sidx[m]:-1, eidx[m]-1:ww0+sidx[m]:-1]
                    q[sidx[m]:eidx[m],ell] += psi[J] * q[sidx[m]:eidx[m],ell-1] * gamma[s] * Gamma_r[ell]

        # calculate worst-case error for every power of 2
        if product_weights:
            S = q[:,0] # alias!
        else:
            S[:] = 0
            for ell in range(1, min(s, qstar)+1):
                S += q[:,ell]
        prev_e2[:] = 0
        for m in range(m2+1):
            prev_e2[m] += r[m] * sum(S[sidx[m]:eidx[m]]) # we do cumsum after the loop
        prev_e2 = cumsum(prev_e2) / 2**arange(m2+1)
        if product_weights: prev_e2 = prev_e2 - 1
        e2s[s0,:] = prev_e2

        elapsed_time2 = os.times()[4]
        cur_t_min = (elapsed_time2 - elapsed_time0) / 60.;
        it_t_min = (elapsed_time2 - elapsed_time1) / 60.;

        if product_weights:
            tt = s
            rem_t_min = (cur_t_min / tt) * (s_max-s)
        else:
            msq = min(s, qstar)
            tt = msq * (msq+1) / 2. + max(s-qstar,0) * qstar
            msq = min(s_max, qstar)
            tt2 = msq * (msq+1) / 2. + max(s_max-qstar,0) * qstar
            rem_t_min = (cur_t_min / tt) * (tt2-tt)

        if verbose > 1 or rem_t_min > 1: 
            print("s=%4d, z=%6d, e2=%.4e, x=%.4e, mloc=%4d" % (s, z[s0], e2[s0], x[s0], mloc))
        if log: 
            with open(log, 'a') as f:
                f.write("%d,%d,%g,%g,%d,%g,%g,%g\n" % (s, z[s0], e2[s0], x[s0], mloc, it_t_min, cur_t_min, rem_t_min))

    return z, e2, x, e2s, CBC_optimal



def propose_lambda(decay, delta=0.125):
    """Note: the return type is either that of decay or that of delta!"""
    import numbers
    if isinstance(decay, numbers.Integral): 
        print("Warning: changed the type of your decay to float")
        decay = float(decay)
    if isinstance(delta, numbers.Integral):
        print("Warning: changed the type of your delta to float")
        delta = float(delta)
    if decay < 1:
        print("Warning: decay should be larger than 1, I've set it to 1 (as a limiting value)")
        one = decay-decay+1
        decay = one
    if (delta <= 0) or (delta >= 0.5): 
        print("Warning: delta should be in (0,1/2), picking closest value in [0+1/16,1/2-1/16]")
        one = delta-delta+1
        delta = min(max(delta, one/16.), 0.5-one/16.)
    if 2*decay > 3 - 2*delta: return 1/(2-2*delta)
    else: return 1/(2*decay-1)

def rho(lambda_):
    return 2*zeta(2*lambda_, 1) / (2*pi**2)**lambda_

def rhoj_Rs(lambda_, alphaj):
    eta = (2*lambda_ - 1)/(4*lambda_)   # 0 < eta < 1 - 1/(2*lambda)
    #print "zeta(%g) = %s, eta = %s" % (lambda_+0.5, zeta(lambda_+0.5,1), eta)
    #print "alphaj = %s, exp(alphaj**2/eta) = %s" % (alphaj, exp(alphaj**2/eta))
    #print (sqrt(2*pi) * exp(alphaj**2/eta) / pi**(2-2*eta) / (1-eta) / eta)**lambda_
    return 2 * (sqrt(2*pi) * exp(alphaj**2/eta) / pi**(2-2*eta) / (1-eta) / eta)**lambda_ * zeta(lambda_+0.5, 1)

def alphaj(betaj, lambda_, a3=1):
    return ( a3*betaj + sqrt((a3*betaj)**2 + 1 - 1 / (2*lambda_)) ) / 2

def Phi(x):
    return erfc(-x/sqrt(2))/2

def PhiInv(x):
    return -sqrt(x-x+2) * erfinv(1-2*x)

def gammaR(betaj, a2, a3, lambda_):
    alphaj_ = alphaj(betaj, lambda_, a3)
    rhoj = rhoj_Rs(lambda_, alphaj_)
    gammaj = ( a2 * betaj / ( 2 * exp((a3 * betaj)**2/2) * Phi(a3 * betaj) * sqrt( (alphaj_ - a3 * betaj) * rhoj ) ) )**(2/(1+lambda_))
    #print "betaj=%g, alphaj=%g, rhoj=%g, gammaj=%g" % (betaj, alphaj_, rhoj, gammaj)
    return gammaj

def thetajhalf(x, alphaj):
    """ FIXME: to be deleted... just for reference to compare
    This is the theta_j(x) function from Nichols & Kuo (2014), eq. (50), for
    0 <= x <= 1/2.  We do not use this version as it involves integrals and we
    can do it (at least partialy) in closed form using the functions a(x,
    alpha_j), b(x, alpha_j), c(alpha_j) below.
    """
    from mpmath import quad
    return 2 * quad(lambda t: (Phi(t)-x) * exp(-2*alphaj*t), [PhiInv(x), 0]) - 2 * quad(lambda t: Phi(t)**2 * exp(-2*alphaj*t), [-inf, 0])

def theta_a(x, alphaj):
    if x == 0.5: return 0
    if x == 0  : return ( -1 + exp(2*alphaj**2) * (1 + erf(sqrt(2)*alphaj)) ) / (2*alphaj)
    return ( -1 + 2 * x * exp(-2*alphaj*PhiInv(x)) + exp(2*alphaj**2) * (erf(sqrt(2)*alphaj) - erf((2*alphaj+PhiInv(x))/sqrt(2))) ) / (2*alphaj)
    #return -1/(2*alphaj) + exp(2*alphaj**2)/(2*alphaj) * (erf(sqrt(2)*alphaj) - erf((2*alphaj+PhiInv(x))/sqrt(2)))

def theta_b(x, alphaj):
    if x == 0: return 0
    return -x * expm1(-2*alphaj*PhiInv(x)) / alphaj
    #return -2 * x * ( exp(-2*alphaj*PhiInv(x)) - 1 ) / (2*alphaj)
    #return -2 * x * exp(-2*alphaj*PhiInv(x)) / (2*alphaj) + 2 * x / (2*alphaj)

def theta_c(alphaj):
    """NB: This function currently still involves an integral and we are
    dependent on the mpmath package for using the quadrature method therefore."""
    if MP:
        from mpmath import quad
        Q = quad(lambda t: exp(-2*alphaj*t - t**2/2) / sqrt(2*pi) * erfc(-t/sqrt(2)), [-inf, 0])
    else:
        from scipy.integrate import quad
        Q, abserr = quad(lambda t: exp(-2*alphaj*t - t**2/2) / sqrt(2*pi) * erfc(-t/sqrt(2)), -inf, 0, epsabs=1e-13)
    return ( 1 - 4 * Q ) / (4*alphaj)

def theta_ab(x, alphaj):
    x = min(x, 1-x) # NOTE: this symmetry could be implied earlier
    return theta_a(x, alphaj) + theta_b(x, alphaj)

def _test():
  import doctest
  doctest.testmod()

if __name__ == "__main__":
  _test()
